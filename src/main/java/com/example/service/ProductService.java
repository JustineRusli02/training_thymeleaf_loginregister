package com.example.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Product;
import com.example.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository pr;
	
	public List<Product> listAll(){
		return pr.findAll();
	}
	
	public void save(Product product) {
		pr.save(product);
	}
	
	public Product get(Long id) {
		return pr.findById(id).get();
	}
	
	public void delete(Long id) {
		pr.deleteById(id);
	}
	
}

