package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.example"})
@EntityScan("com.example.model")
@EnableJpaRepositories("com.example.repository")
@EnableAutoConfiguration
public class SpringLoginRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLoginRegisterApplication.class, args);
	}

}
