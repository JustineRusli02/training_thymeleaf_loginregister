package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.model.Product;
import com.example.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	private ProductService ps;
	
	@GetMapping("home/index")
	public String viewHomePage(Model model) {
		List<Product> listProducts = ps.listAll();
		model.addAttribute("listProducts",listProducts);
		
		return "home/home";
	}
	
	@GetMapping("/newProduct")
	public String showNewProductPage(Model model) {
		Product product = new Product();
		model.addAttribute("product", product);
		
		return "home/new_product";
	}
	
	@PostMapping("/saveProduct")
	public String saveProduct(@ModelAttribute("product") Product product) {
		ps.save(product);
		
		return "redirect:/";
	}
	
	@GetMapping("/edit/{productId}")
	public ModelAndView showEditProductPage(@PathVariable Long productId) {
		ModelAndView mav = new ModelAndView("home/edit_product");
		Product product = ps.get(productId);
		mav.addObject("product", product);
		
		return mav;
	}
	
	@GetMapping("/delete/{productId}")
	public String deleteProduct(@PathVariable Long productId) {
		ps.delete(productId);
		
		return "redirect:/";
	}
	
	
}
