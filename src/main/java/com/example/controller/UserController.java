package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.model.Role;
import com.example.model.User;
import com.example.repository.RoleRepository;
import com.example.service.ProductService;
import com.example.service.UserService;


@Controller
public class UserController {

	@Autowired
	private UserService us;
	
	@Autowired
	private RoleRepository rr;
	
	@GetMapping("/")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("user/login");
		return mav;
	}
	
	@GetMapping("user/signup")
	public ModelAndView signup() {
		
		ModelAndView model = new ModelAndView();
		User user = new User();
		List<Role> roleList = rr.findAll();
		model.addObject("roleList", roleList);
		model.addObject("user", user);
		model.setViewName("user/signup");

		return model;
		
	}
	
	@PostMapping("signup")
	public ModelAndView createUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();
		User userExists = us.findByEmail(user.getEmail());

		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user", "This email already exists!");
		}
		if (bindingResult.hasErrors()) {
			model.setViewName("user/signup");
		} else {
			us.saveUser(user);
			model.addObject("msg", "User has been registered successfully!");
			model.addObject("user", new User());
			model.setViewName("home/home");
		}
		return model;
	}
	
	@GetMapping("/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = us.findByEmail(auth.getName());
		Role role = user.getRole();

//		if (role.getRole() != "ADMIN") {
//			model.setViewName("errors/access_denied");
//			return model;
//		}

		model.addObject("userName", user.getFirstName() + " " + user.getLastName());
		model.addObject("roleName", role.getRole());
		model.setViewName("home/home");
		return model;
	}
	
	
}
